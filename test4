import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Value("${login.enabled}")
    private Boolean loginEnabled;

    @Autowired
    private JWTRequestFilter jwtRequestFilter;

    @Value("${pivot.forms.cookie.name}")
    private String accessTokenCookieName;

    @Configuration
    public static class WebSecurityConfig extends AbstractHttpConfigurer<WebSecurityConfig, HttpSecurity> {

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.headers(headers -> headers.frameOptions(frameOptions -> frameOptions.sameOrigin()))
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .csrf().disable();

            if (loginEnabled) {
                http.requestMatchers()
                    .antMatchers("/api**").permitAll()
                    .antMatchers("/pivot/**").authenticated();
                http.addFilterBefore(new JWTRequestFilter(), UsernamePasswordAuthenticationFilter.class);
            } else {
                http.authorizeRequests().antMatchers("/").permitAll();
            }
        }
    }
}
